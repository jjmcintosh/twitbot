
import tweepy 
import config
import time
import random
import re 


auth = tweepy.OAuth1UserHandler( config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret
)


pattern = re.compile("@[^\s]*(Phyrexia|phyrexia)[^\s]*")



api_m = tweepy.Client(
    bearer_token = config.bearer_token,
    consumer_key = config.consumer_key,
    consumer_secret = config.consumer_secret,
    access_token = config.access_token,
    access_token_secret = config.access_token_secret
    )


bot_id = 1581303071736565760

bot_name = "@PhyrexiaEmbassy"

# def tweeting_function():
#     #The message that will become a tweet
#     print("Sending Tweet!")
#     message = "This is an automated tweet!"
#     api_m.update_status(status = message)

# tweeting_function()

# raise ValueError


def commenting_function(twt):

    tweet_id = twt.id
    tweet_text = twt.text.lower()
    author_id = twt.author_id



    messages = ["""Thank you for your interest in Phyrexia. We are a multi-planar civilization striving for perfection.
Have you been compleated yet? If not, we have sleeper agents standing by to assist you.
All Will Be фne""",
"""Your interest in Phyrexia has been noted. We are sending sleeper agents your way to assist in your compleation.
All Will Be фne"""
"""Have you considered compleation? Talk to your local sleeper agent today for more information.
All Will Be фne""",
"""Our sleeper agents are standing by to assist in your compleation.
All Will Be фne""",
"""Need a vacation? Come visit New Phyrexia! Immerse yourself in the culture of the Machine Orthodoxy and worship the Argent Etchings.
All Will Be фne"""]


    message = random.choice(messages)


    print("Tweet Found in stream!")
   
    print(f"TWEET:{tweet_text}")
    
    if (author_id != bot_id):

        if ('phyrexia' in tweet_text and not pattern.findall(tweet_text)):
            #another try/except to see if we're able to comment. If not, an error is printed to the console.
            try:
                print("Attempting Comment")
                api_m.create_tweet(text = message, in_reply_to_tweet_id = tweet_id,)
                print("TWEET SUCCESSFUL")
            except Exception as err:
                print(err)
        else:
            print("Tweet wasn't nice enough")
    #else statement for the logic that keeps this from firing on retweets, quote tweets, and posts by the bot itself. 
    else:
        print("Could not comment: RT, QT, or bot tweet")

class MyStreamListener(tweepy.StreamingClient):

    def on_connect(self):

        print("Connected")


    def on_tweet(self, twts):
        
        commenting_function(twts)

        time.sleep(10)           

    def on_error(self, status):
        print(status)



stream_m = MyStreamListener(wait_on_rate_limit= True, bearer_token= config.bearer_token)



#[stream_m.delete_rules(rule) for rule in stream_m.get_rules().data]

stream_m.add_rules(tweepy.StreamRule("Phyrexia -is:retweet -is:quote"))

print("Rules", stream_m.get_rules())

stream_m.filter(tweet_fields=['author_id'])






